;;; init --- Publish or tangle org files

;; Author: Alessio Caiazza
;; Forked from https://gitlab.com/to1ne/literate-dotfiles

;;; Commentary:

;; This file boostraps straight.el loading dependencies and configuring Emacs.
;; It provides a function for tangling and one for publishing HTML pages.

;;; Code:

;;;; Emacs bootstrap

(setq debug-on-error t)

;; this is designed to be invoked from make and using a local emacs config
(setq user-emacs-directory ".config/emacs")

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq straight-vc-git-default-clone-depth 1)

(straight-use-package 'org :branch "main")
(require 'ob-tangle)
(require 'ox-publish)

;; evaluate emacs-lisp blocks without asking for a confirmation
(setq org-confirm-babel-evaluate nil)

(straight-use-package 'fish-mode)
(straight-use-package 'htmlize)

;; org-babel languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (sqlite .t)
   (shell . t)))

;;;; tangle

;; This function can be used to tangle one or more files to their output
;; files.

(defun literate-dotfiles-tangle (&rest files)
  "Tangle FILES or all files in the project."
  (when (null files)
    (setq files command-line-args-left))
  (dolist (file files)
    (with-current-buffer (find-file-noselect file)
      (org-babel-tangle))))

;;;; publish

;; This file takes care of exporting org files to the public directory.
;; Images and such are also exported without any processing.

(defun literate-dotfiles--postamble-format (info)
  "Function that formats the HTML postamble.
INFO is a plist used as a communication channel."
  (let* ((base-dir (getenv "CI_PROJECT_DIR"))
	 (input-file (file-relative-name (plist-get info :input-file) base-dir))
	 (html-file (file-relative-name (plist-get info :output-file) (concat base-dir "public"))))
    (concat "<hr><p class=\"Source\">Source: "
     (format "<a rel=\"syndication\" class=\"u-syndication\" href=\"%s/%s\">%s</a></p>"
             "https://gitlab.com/nolith/my-878uv-codeplug/blob/main"
             input-file input-file)
     "<a class=\"p-author h-card\" rel=\"author\" alt=\"Alessio Caiazza\" href=\"https://abisso.org\"></a>"
     (format "<a class=\"u-url\" href=\"https://nolith.gitlab.io/my-878uv-codeplug/%s\"></a>" html-file)
     (unless (equal input-file "README.org")
	"<p>Return to <a href=\"/my-878uv-codeplug/index.html\">index</a>.</p>"))))

(defvar literate-dotfiles--site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js"
                "eot" "woff" "woff2" "ttf"
                "html" "pdf"))
  "File types that are published as static files.")

(defvar literate-dotfiles--publish-project-alist
      (list
       (list "site-org"
             :base-directory "."
             :base-extension "org"
             :recursive t
             :publishing-function 'org-html-publish-to-html
             :publishing-directory "./public"
             :exclude (regexp-opt '("\.config/"))
             :auto-sitemap t
             :html-head-include-default-style nil
             :html-head-include-scripts nil
             :html-htmlized-css-url "/my-878uv-codeplug/org.css"
             :html-head-extra (concat
			       "<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.ico\" />"
			       "<link rel=\"webmention\" href=\"https://webmention.io/abisso.org/webmention\">"
			       "<link rel=\"pingback\" href=\"https://webmention.io/abisso.org/xmlrpc\">")
             :html-postamble 'literate-dotfiles--postamble-format
             :sitemap-style 'list
             :sitemap-sort-files 'anti-chronologically)
       (list "site-static"
             :base-directory "."
             :exclude (regexp-opt '("public/" "img/design/"))
             :base-extension literate-dotfiles--site-attachments
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site"
             :components '("site-org"))
       ))

(defun literate-dotfiles-publish-all ()
  "Publish the literate dotfiles to HTML."
  (interactive)
  (let ((org-publish-project-alist       literate-dotfiles--publish-project-alist)
        (org-publish-timestamp-directory "./.timestamps/")
;;      (user-full-name                  nil) ;; avoid "Author: x" at the bottom
        (org-export-with-section-numbers nil)
        (org-export-with-smart-quotes    t)
        (org-export-with-toc             nil)
        (org-html-divs '((preamble  "header" "top")
                         (content   "main"   "content")
                         (postamble "footer" "postamble")))
        (org-html-container-element         "section")
        (org-html-metadata-timestamp-format "%Y-%m-%d")
        (org-html-checkbox-type             'html)
        (org-html-html5-fancy               t)
        (org-html-validation-link           nil)
        (org-html-doctype                   "html5")
        (org-html-htmlize-output-type       'css))
    (setq org-html-htmlize-output-type 'css)
    ;; when not running in CI I'm setting this variable to the current
    ;; directory. With this as I can easily get the org file paths
    ;; from the repository's root
    (if (eql (length (getenv "CI_PROJECT_DIR"))
	     0)
	(setenv "CI_PROJECT_DIR"  (expand-file-name default-directory)))
    (org-publish-all)))

;; codeplug generation

(defun make-codeplug ()
  "Generate the codeplug."
  (org-babel-execute-buffer
   (find-file "README.org")))
;;; init.el ends here
