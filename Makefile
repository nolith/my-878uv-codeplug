mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))
cache_dir := .cache
publish_dir := public
timestamps_dir := .timestamps
docs := README.org sitemap.org
orgs := $(filter-out $(docs), $(shell git ls-files \*.org))
emacs_pkgs := org

init_el := elisp/init.el

^el = $(filter %.el,$^)
EMACS = emacs --no-init-file --load $(init_el)
EMACS.funcall = $(EMACS) --batch --funcall
SED ?= $(shell which gsed || which sed)

all: tools codeplug publish

test: tangle

tools: codeplug-diff

codeplug: $(init_el) README.org
	$(EMACS.funcall) make-codeplug

publish: $(init_el) $(orgs)
	$(EMACS.funcall) literate-dotfiles-publish-all
	$(SED) -i -e 's|^<body>|<body class="h-entry">|'\
		-e 's|^<h1 class="title">|<h1 class="title p-title">|'\
		-e 's|^<main |<main class="e-content p-name" |'\
		public/*.html

codeplug-diff: $(basename README.org)

clean:
	rm -rf $(publish_dir)
	rm -rf $(timestamps_dir)
	rm -rf $(cache_dir)

edit:
	$(EMACS) README.org

tangle: $(basename $(orgs))

# https://emacs.stackexchange.com/a/27128/2780
$(cache_dir)/%.out: %.org $(init_el) $(cache_dir)/
	$(EMACS.funcall) literate-dotfiles-tangle $< > $@

%/:
	mkdir -p $@

%: %.org $(cache_dir)/%.out
	@$(NOOP)

.PHONY: all clean edit codeplug
.PRECIOUS: $(cache_dir)/ $(cache_dir)/%.out
